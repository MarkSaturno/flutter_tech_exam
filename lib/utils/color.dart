import 'package:flutter/material.dart';

const Color tabBarColorBorder = Color.fromRGBO(132, 101, 255, 1);

const Color textFieldBackground = Color.fromRGBO(245, 245, 245, 1);

//Time Picker
const Color timePickerBorder = Color.fromRGBO(76, 183, 230, 1);

//Time Tracking

const Color iconColorBackgroudRed = Color.fromRGBO(255, 65, 65, 1);
const Color iconColorBackgroudOrange = Color.fromRGBO(255, 183, 43, 1);
const Color iconColorBackgroudBlue = Color.fromRGBO(103, 136, 255, 1);
const Color iconColorBackgroudViolet = Color.fromRGBO(132, 101, 240, 1);
const Color iconColorBackgroudGrey = Color.fromRGBO(237, 237, 237, 1);

//Add Time Tracking

const Color dropDownColor = Color.fromRGBO(224, 224, 224, 1);

//Drawer

const Color underlineColorViolet = Color.fromRGBO(132, 101, 255, 1);

const Color textColorGrey = Color.fromRGBO(185, 185, 185, 1);
