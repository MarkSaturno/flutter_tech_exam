import 'package:flutter/material.dart';

import 'color.dart';

final defaultTheme = ThemeData(
  primaryColor: Colors.white,
  textTheme: TextTheme(
    bodyText1: TextStyle(
      fontSize: 14,
      fontFamily: 'Roboto',
      fontWeight: FontWeight.bold,
    ),
    bodyText2: TextStyle(
      fontSize: 14,
      fontFamily: 'Mullish',
    ),
    headline1: TextStyle(
      fontSize: 16,
      fontFamily: 'Allerta Stencil',
      color: Colors.black,
    ),
    headline2: TextStyle(
      fontSize: 16,
      fontFamily: 'Allerta Stencil',
    ),
    headline3: TextStyle(
      fontSize: 22,
      fontFamily: 'Allerta Stencil',
      color: Colors.black,
    ),
    headline4: TextStyle(
      fontSize: 12,
      fontFamily: 'Allerta Stencil',
      color: Colors.black,
    ),
    headline5: TextStyle(
      fontFamily: 'Mullish',
      fontSize: 12,
      color: Colors.grey,
    ),
    headline6: TextStyle(
      fontFamily: 'Roboto',
      fontSize: 16,
      color: textColorGrey,
    ),
  ),
);
