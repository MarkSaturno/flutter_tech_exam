import 'package:flutter/material.dart';

import '../widgets/drawer.dart';
import '../widgets/tab_bar_view_meine_vorgesetzte/user_details.dart';

class BusinessCard extends StatefulWidget {
  static const routeName = '/visitenkarte';

  @override
  _BusinessCardState createState() => _BusinessCardState();
}

class _BusinessCardState extends State<BusinessCard> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
          backgroundColor: Color.fromRGBO(236, 236, 236, 1),
          appBar: AppBar(
            backgroundColor: Theme.of(context).primaryColor,
          ),
          drawer: MainDrawer(),
          body: Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 20,
              horizontal: 15,
            ),
            child: Column(
              children: [
                TabBar(
                  indicator: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('./assets/images/lines.png'),
                        fit: BoxFit.cover,
                      ),
                      border: Border(
                          bottom: BorderSide(
                        color: Color.fromRGBO(132, 101, 255, 1),
                        width: 3,
                      ))),
                  tabs: [
                    Tab(text: 'Meine Visitenkarte'),
                    Tab(text: 'Vorgensetzte'),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  decoration: BoxDecoration(color: Colors.white),
                  height: 505.42,
                  child: UserDetailsShow(),
                )
              ],
            ),
          )),
    );
  }
}
