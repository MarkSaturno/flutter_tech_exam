import 'package:flutter/material.dart';
import 'package:time_tracking_app/utils/color.dart';
import 'package:time_tracking_app/utils/constants.dart';
import 'package:time_tracking_app/widgets/exm_icon_button.dart';
import 'package:time_tracking_app/widgets/theme_styled_text_widgets.dart';

import 'add_time_recording_screen.dart';
import 'calendar_screen.dart';
import '/widgets/drawer.dart';

class TimeRecordingScreen extends StatelessWidget {
  static const routeName = '/time-tracking';

  void _navigateToCalendarScreen(BuildContext context) => Navigator.of(context).pushNamed(CalendarScreen.routeName);
  void _navigateToAddTimeTrackingScreen(BuildContext context) =>
      Navigator.of(context).pushNamed(AddTimeRecording.routeName);

  @override
  Widget build(BuildContext context) {
    final imageSendKlein = Image.asset(
      './assets/images/Union.png',
      width: 40,
      height: 40,
    );
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        toolbarHeight: appBarHeight,
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Donnerstag',
              style: Theme.of(context).textTheme.headline3,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Image.asset(
                  './assets/images/status.png',
                  width: 60,
                  height: 30,
                ),
                Text(
                  '12.01.2021',
                  style: Theme.of(context).textTheme.headline6,
                ),
              ],
            )
          ],
        ),
        actions: [
          EXMIconButton(
            imageUrl: './assets/images/calendar-button.png',
            onPressed: () => _navigateToCalendarScreen(context),
          ),
          EXMIconButton(
            imageUrl: './assets/images/add-button.png',
            onPressed: () => _navigateToAddTimeTrackingScreen(context),
          ),
        ],
      ),
      drawer: MainDrawer(),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: mainContainerHeight,
            color: Colors.white,
            width: double.infinity,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: [
                    Container(
                      height: containerHeight,
                      child: FloatingActionButton(
                        heroTag: null,
                        onPressed: () {
                          // Add your onPressed code here!
                        },
                        child: imageSendKlein,
                        backgroundColor: iconColorBackgroudRed,
                      ),
                    ),
                    const SizedBox(height: 5),
                    HeadLine4(text: 'Sa')
                  ],
                ),
                Column(
                  children: [
                    Container(
                      height: containerHeight,
                      child: FloatingActionButton(
                        heroTag: null,
                        onPressed: () {
                          // Add your onPressed code here!
                        },
                        child: imageSendKlein,
                        backgroundColor: iconColorBackgroudOrange,
                      ),
                    ),
                    const SizedBox(height: 5),
                    HeadLine4(text: 'Di')
                  ],
                ),
                Column(
                  children: [
                    Container(
                      height: containerHeight,
                      child: FloatingActionButton(
                        heroTag: null,
                        onPressed: () {
                          // Add your onPressed code here!
                        },
                        child: imageSendKlein,
                        backgroundColor: iconColorBackgroudBlue,
                      ),
                    ),
                    const SizedBox(height: 5),
                    HeadLine4(text: 'Mi')
                  ],
                ),
                Column(
                  children: [
                    Container(
                      height: containerHeight,
                      child: FloatingActionButton(
                        heroTag: null,
                        onPressed: () {
                          // Add your onPressed code here!
                        },
                        child: Image.asset(
                          './assets/images/edit-klein-icon.png',
                          width: 40,
                          height: 40,
                        ),
                        backgroundColor: iconColorBackgroudViolet,
                      ),
                    ),
                    const SizedBox(height: 5),
                    HeadLine4(text: 'Do')
                  ],
                ),
                Column(
                  children: [
                    Container(
                      height: containerHeight,
                      child: FloatingActionButton(
                        heroTag: null,
                        onPressed: () {
                          // Add your onPressed code here!
                        },
                        child: Text('Fr'),
                        backgroundColor: iconColorBackgroudGrey,
                      ),
                    ),
                    const SizedBox(height: 5),
                    HeadLine4(text: 'Fr')
                  ],
                ),
                Column(
                  children: [
                    Container(
                      height: containerHeight,
                      child: FloatingActionButton(
                        heroTag: null,
                        onPressed: () {
                          // Add your onPressed code here!
                        },
                        child: Text('Sa'),
                        backgroundColor: iconColorBackgroudGrey,
                      ),
                    ),
                    const SizedBox(height: 5),
                    HeadLine4(text: 'Sa')
                  ],
                ),
                Column(
                  children: [
                    Container(
                      height: containerHeight,
                      child: FloatingActionButton(
                        heroTag: null,
                        onPressed: () {
                          // Add your onPressed code here!
                        },
                        child: Text('So'),
                        backgroundColor: iconColorBackgroudGrey,
                      ),
                    ),
                    const SizedBox(height: 5),
                    HeadLine4(text: 'So')
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
