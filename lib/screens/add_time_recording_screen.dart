import 'package:flutter/material.dart';
import 'package:time_tracking_app/screens/pause_screen.dart';
import 'package:time_tracking_app/utils/color.dart';
import 'package:time_tracking_app/utils/constants.dart';
import 'package:time_tracking_app/widgets/add_user.dart';
import 'package:time_tracking_app/widgets/add_time_tracking/save_button.dart';
import 'package:time_tracking_app/widgets/exm_dropdown_button.dart';
import 'package:time_tracking_app/widgets/theme_styled_text_widgets.dart';
import 'package:time_tracking_app/widgets/time_picker.dart';

class AddTimeRecording extends StatefulWidget {
  static const routeName = '/add-time-tracking';
//   String? dropDownValue;
//   AddZeiterFassung({this.dropDownValue});
  @override
  _AddTimeRecordingState createState() => _AddTimeRecordingState();
}

class _AddTimeRecordingState extends State<AddTimeRecording> {
  void _navigateToPauseScreen(BuildContext context) => Navigator.of(context).pushNamed(PauseScreen.routeName);
  void _returnToTimeTrackingScreen(BuildContext context) => Navigator.of(context).pop();

  String? _projectsValue;
  String? _projectNummerValue;
  String? _projectQuality;
  final List<String> userListData = [];

  static const List<String> _projects = [
    'Project1',
    'Project2',
  ];

  static const List<String> _projectNummer = [
    '1',
    '2',
  ];

  static const List<String> _userData = [
    'Aarone',
    'Hebe',
    'Jan',
  ];

  void _changeCategoryValue(newValue) {
    setState(() => _projectsValue = newValue);
  }

  void _changeProjectNumberValue(newValue) {
    setState(() => _projectNummerValue = newValue);
  }

  void _addUser(newValue) {
    setState(() {
      _projectQuality = newValue;

      if (_projectQuality != null && userListData.indexOf(_projectQuality!) == -1) {
        userListData.add(_projectQuality!);
      }

      Navigator.pop(context);
    });
  }

// Note: Not included on this task ticket
//   Widget cardButton(String text, String subTitle, Color color, bool logic) {
//     return Container(
//       padding: const EdgeInsets.only(
//         top: 10,
//         bottom: 10,
//         left: 10,
//       ),
//       child: Row(
//         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//         children: [
//           Column(
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: [
//               Text(text, style: TextStyle(fontFamily: 'Allerta Stencil', fontSize: 22)),
//               widget.dropDownValue != null && logic == true
//                   ? Container(
//                       padding: EdgeInsets.all(10),
//                       decoration: BoxDecoration(
//                           border: Border.all(),
//                           borderRadius: BorderRadius.all(Radius.circular(20)),
//                           color: Colors.black),
//                       child: Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: [
//                           Align(
//                             alignment: Alignment.center,
//                             child: Text(
//                               widget.dropDownValue as String,
//                               style: TextStyle(fontFamily: 'Mulish', fontSize: 16, color: Colors.white),
//                             ),
//                           ),
//                           InkWell(
//                               onTap: () {
//                                 setState(() {
//                                   widget.dropDownValue = null;
//                                 });
//                               },
//                               child: Image.asset('./assets/images/close-klein-light.png'))
//                         ],
//                       ))
//                   : Text(subTitle)
//             ],
//           ),
//           logic == true
//               ? Container()
//               : FloatingActionButton(
//                   elevation: 0,
//                   onPressed: () {},
//                   child: Image.asset('./assets/images/plus-klein-light.png'),
//                   backgroundColor: color)
//         ],
//       ),
//     );
//   }

  final keyForm = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    final backgroundColorWhite = Theme.of(context).primaryColor;
    // Note: Not included on this task ticket
    // final userData = Provider.of<UserData>(context).userList;
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          toolbarHeight: appBarHeight,
          leading: InkWell(
            onTap: () => _returnToTimeTrackingScreen(context),
            child: Image.asset(
              './assets/images/close-icon.png',
              width: 42,
              height: 42,
            ),
          ),
          actions: [
            Container(
              padding: const EdgeInsets.only(right: 20),
              child: Image.asset(
                './assets/images/logo.png',
                width: 23.94,
                height: 39.89,
              ),
            )
          ],
          elevation: 0,
        ),
        body: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          color: backgroundColorWhite,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TabBar(
                indicator: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('./assets/images/lines.png'),
                    fit: BoxFit.cover,
                  ),
                  border: Border(
                    bottom: BorderSide(
                      color: tabBarColorBorder,
                      width: 3,
                    ),
                  ),
                ),
                tabs: [
                  Tab(
                    child: Text(
                      'Arbeitszeit',
                      style: Theme.of(context).textTheme.headline1,
                    ),
                  ),
                  Tab(
                    child: InkWell(
                      onTap: () => _navigateToPauseScreen(context),
                      child: Text(
                        'Pause',
                        style: Theme.of(context).textTheme.headline2,
                      ),
                    ),
                  )
                ],
              ),
              const SizedBox(height: 25),
              Expanded(
                child: TabBarView(
                  children: [
                    SingleChildScrollView(
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            HeadLine3(text: 'Kategorie'),
                            const SizedBox(height: 10),
                            EXMDropdownButton(
                              projectsData: _projects,
                              value: _projectsValue,
                              placeHolder: 'Wahlen Sie bitte Kategorie aus',
                              onPressed: (newValue) => _changeCategoryValue(newValue),
                            ),
                            const SizedBox(height: 20),
                            HeadLine3(text: 'Projektnummer'),
                            const SizedBox(height: 10),
                            EXMDropdownButton(
                              projectsData: _projectNummer,
                              value: _projectNummerValue,
                              placeHolder: 'Projektnummer hinzufügen',
                              onPressed: (newValue) => _changeProjectNumberValue(newValue),
                            ),
                            const SizedBox(height: 34),
                            AddUser(
                              projectsData: _userData,
                              placeHolder: 'Choose a user',
                              value: _projectQuality,
                              onPressed: _addUser,
                              userList: userListData,
                            ),
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      // Note: Not included on this task ticket
                                      // cardButton('Mitarbeiter', 'hinzufügen oder bearbeiten', Colors.black, true),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(height: 34),
                            Padding(
                              padding: const EdgeInsets.only(left: 8.0),
                              child: Text(
                                'Arbeitszeit',
                                style: Theme.of(context).textTheme.headline3,
                              ),
                            ),
                            TimePickerView(),
                            //   Note: Not included on this task ticket
                            //   widget.dropDownValue != null
                            //       ? Column(
                            //           children: [
                            //             cardButton('Pause', 'hinzufügen oder bearbeiten',
                            //                 Color.fromRGBO(103, 136, 255, 1), false),
                            //             cardButton('Wartezeit', 'hinzufügen oder bearbeiten',
                            //                 Color.fromRGBO(255, 183, 43, 1), false),
                            //             cardButton('Bereitschaftszeit', 'hinzufügen oder bearbeiten',
                            //                 Color.fromRGBO(103, 136, 255, 1), false),
                            //           ],
                            //         )
                            //       : Container(),
                            const SizedBox(height: 34),
                            Container(
                              height: 150,
                              color: textFieldBackground,
                              child: TextField(
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: 'Enter a search term',
                                  prefixIcon: Padding(
                                    padding: const EdgeInsets.all(10),
                                    child: Image.asset(
                                      './assets/images/user-icon.png',
                                      width: 90,
                                      height: 80,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(height: 32),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 40.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  //   Note: Not included on this task ticket
                                  // AbbrechenBtn(),
                                  SaveButton(keyForm: keyForm),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
