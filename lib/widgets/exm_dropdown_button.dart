import 'package:flutter/material.dart';

import 'package:time_tracking_app/utils/color.dart';

class EXMDropdownButton extends StatefulWidget {
  final List<String> projectsData;
  final String placeHolder;
  final String? value;
  final Function onPressed;

  EXMDropdownButton({
    required this.projectsData,
    required this.value,
    required this.placeHolder,
    required this.onPressed,
  });

  @override
  _EXMDropdownButtonState createState() => _EXMDropdownButtonState();
}

class _EXMDropdownButtonState extends State<EXMDropdownButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 55,
      width: 350,
      child: DropdownButton(
        value: widget.value,
        hint: Text(widget.placeHolder),
        isExpanded: true,
        icon: Image.asset('assets/images/arrow-double-klein.png'),
        iconSize: 24,
        elevation: 50,
        underline: Container(
          height: 3,
          color: dropDownColor,
        ),
        onChanged: (_projectsValue) => widget.onPressed(_projectsValue),
        items: widget.projectsData
            .map<DropdownMenuItem<String>>(
              (String value) => DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              ),
            )
            .toList(),
      ),
    );
  }
}
