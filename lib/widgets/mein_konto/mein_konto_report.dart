import 'package:flutter/material.dart';

class MeinKontoReport extends StatelessWidget {

    final String title;
    final String text;
    final String buttonText;
  
    MeinKontoReport({required this.title,required this.text, required this.buttonText});
    
    @override
    Widget build(BuildContext context) {
        return Container(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    Text(
                        title,
                        style: TextStyle(
                            fontFamily: 'Allerta Stencil',
                            fontSize: 22, 
                        )
                    ),
                    Row(
                        children: [
                            Image.asset('./assets/images/calendar.png', height: 60,),
                            Container(
                                padding: EdgeInsets.only(top: 30, left: 20),
                                child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                        Text(
                                            text,
                                            style: TextStyle(
                                                fontFamily: 'Allerta Stencil',
                                                fontSize: 16
                                            ),
                                        ),
                                        SizedBox(height: 10,),
                                        Container(
                                            height: 40,
                                            width: 200,
                                            color:Colors.black,
                                            child: Row(
                                                children: [
                                                    TextButton(
                                                        onPressed: (){}, 
                                                        child: Text(
                                                            buttonText, 
                                                            style: TextStyle(
                                                                color: Colors.white,
                                                                fontFamily: 'Roboto',
                                                                fontSize: 14
                                                            ),
                                                        ), 
                                                    ),
                                                    Image.asset('./assets/images/send-klein.png'),
                                                ],
                                            ),
                                        ),
                                        
                                    ],
                                ),
                            )
                        ],
                    ),
                ],
            ),
        );
    }
}