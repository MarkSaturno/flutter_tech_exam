import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:time_tracking_app/utils/color.dart';
import 'timePicker.dart';

class TimePickerView extends StatelessWidget {
  const TimePickerView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: timePickerBorder,
                width: 5,
              ),
            ),
          ),
          child: Row(
            children: [
              RotatedBox(
                quarterTurns: 3,
                child: Icon(FontAwesomeIcons.angleDoubleDown),
              ),
              timePicker(),
            ],
          ),
        ),
        const SizedBox(width: 10),
        Container(
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: timePickerBorder,
                width: 5,
              ),
            ),
          ),
          child: Row(
            children: [
              RotatedBox(
                quarterTurns: 3,
                child: Icon(FontAwesomeIcons.stop),
              ),
              timePicker(),
            ],
          ),
        ),
      ],
    );
  }
}
