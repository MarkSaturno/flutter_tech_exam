import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:time_tracking_app/providers/icon_data.dart';
import 'package:time_tracking_app/screens/business_card_screen.dart';
import 'package:time_tracking_app/screens/time_recording_screen.dart';
import 'package:time_tracking_app/utils/color.dart';

import '/screens/my_account_screen.dart';

class IconItem extends StatefulWidget {
  final String id;
  final String title;
  final String imageUrl;
  late bool isActive;

  IconItem(this.id, this.title, this.imageUrl, this.isActive);

  @override
  _IconItemState createState() => _IconItemState();
}

class _IconItemState extends State<IconItem> {
  void pageNavigation(id, context) {
    switch (id) {
      case "1":
        Navigator.of(context).pushNamed(MyAccount.routeName);
        break;
      case "2":
        Navigator.of(context).pushNamed(BusinessCard.routeName);
        break;
      case "3":
        Navigator.of(context).pushNamed(TimeRecordingScreen.routeName);
        break;
      case "4":
        Navigator.of(context).pushNamed(MyAccount.routeName);
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    final id = Provider.of<IconsDrawer>(context);
    final currentItem = Provider.of<IconsDrawer>(context).navigationId;
    final isSelected = currentItem == widget.id;

    return InkWell(
      onTap: () {
        setState(() => id.setNavigationId(widget.id));
        pageNavigation(widget.id, context);
      },
      child: Container(
        height: 103,
        width: 130,
        child: Column(
          children: [
            Image.asset(
              widget.imageUrl,
              color: isSelected ? Colors.black : Colors.grey,
            ),
            Container(
              decoration: isSelected
                  ? BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          color: underlineColorViolet,
                          width: 3,
                        ),
                      ),
                    )
                  : null,
              child: Text(
                widget.title,
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 16,
                  color: isSelected ? Colors.black : Colors.grey,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
