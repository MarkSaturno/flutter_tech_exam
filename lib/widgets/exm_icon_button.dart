import 'package:flutter/material.dart';

class EXMIconButton extends StatelessWidget {
  final String imageUrl;
  final VoidCallback onPressed;

  EXMIconButton({required this.imageUrl, required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      iconSize: 50,
      icon: Image.asset(
        imageUrl,
        width: 80,
        height: 80,
      ),
      color: Colors.black,
      onPressed: onPressed,
    );
  }
}
