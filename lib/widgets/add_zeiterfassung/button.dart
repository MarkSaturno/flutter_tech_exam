import 'package:flutter/material.dart';

Widget button (String text, String ? imageUrl, Color color, Color colorText, ){
    return Container(
        height: 42,
        width: 113,
        color: color,
        child: Row(
            children: [
                TextButton(
                    onPressed: (){}, 
                    child: Text(
                        text, 
                        style: TextStyle(
                            color: colorText,
                            fontFamily: 'Roboto',
                            fontSize: 14
                        ),
                    ), 
                ),
                imageUrl != null ? Image.asset(imageUrl) : Container(),
            ],
        ),
    );
}