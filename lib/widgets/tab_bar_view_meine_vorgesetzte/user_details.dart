import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '/providers/user_data.dart';
import '/widgets/user.dart';

import 'address_and_contact.dart';

class UserDetailsShow extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        final userData = Provider.of<UserData>(context).user;
        return(
            TabBarView(
                children: [
                    SingleChildScrollView(
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                                Container(
                                    padding: EdgeInsets.only(top:32, bottom: 16, left: 20),
                                    child: Text(
                                        'VisitenKarte',
                                        style: 
                                        TextStyle(
                                            fontFamily: 'Allerta Stencil',
                                            fontSize: 25, 
                                        )
                                    ),
                                ),
                                Container(
                                    padding: EdgeInsets.only(left:16),
                                    child: UserDetails(
                                      imageUrl: userData[0].imageUrl,
                                      name: userData[0].name, email: 
                                      userData[0].email, 
                                      position: userData[0].position,
                                  ),
                                ),
                                
                                Center(
                                    child: Image.asset('./assets/images/qr-code.png',  height: 244, width: 244,),
                                ),
                                SizedBox(height: 36,),
                                Container(
                                    child: AddressAndContact(userData[0].email)
                                ),
                            ],
                        )
                    ),
                    SingleChildScrollView(
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                                Container(
                                    padding: EdgeInsets.only(top:32, bottom: 16, left: 20),
                                    child: Text(
                                        'Vorgesetzte',
                                        style: 
                                        TextStyle(
                                            fontFamily: 'Allerta Stencil',
                                            fontSize: 25, 
                                        )
                                    ),
                                ),
                                Container(
                                    padding: EdgeInsets.only(left:16),
                                    child: UserDetails(
                                        imageUrl: userData[1].imageUrl, 
                                        name: userData[1].name, 
                                        email: userData[1].email, 
                                        position: userData[1].position,
                                    )),
                                Center(
                                    child: Image.asset('./assets/images/qr-code.png',  height: 244, width: 244,),
                                ),
                                SizedBox(height: 36,),
                                Container(
                                    child: AddressAndContact(userData[0].email)
                                ),
                            ],
                        )
                    )
                ]
            )
        );
    }
}