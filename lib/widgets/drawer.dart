import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:time_tracking_app/widgets/icon_item.dart';
import '../providers/icon_data.dart';

class MainDrawer extends StatefulWidget {
  @override
  _MainDrawerState createState() => _MainDrawerState();
}

class _MainDrawerState extends State<MainDrawer> {
    @override
    Widget build(BuildContext context) {
        final iconData = Provider.of<IconsDrawer>(context).icon; 
        return Drawer(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                            Container(
                                margin: EdgeInsets.fromLTRB(16, 44, 0, 0),
                                child: InkWell(
                                    onTap: (){
                                        Navigator.of(context).pop();
                                    },
                                    child: Image.asset(
                                        './assets/images/close-icon.png',
                                        width: 42, 
                                        height: 42,
                                    )
                                )
                            ),
                            Container(
                                margin: EdgeInsets.fromLTRB(0, 48.83, 40.64, 0),
                                child: Image.asset(
                                    './assets/images/logo.png', 
                                    height: 31.91, 
                                    width: 19.15,
                                ),
                            )
                        ],
                    ),
                    SizedBox(height: 10,),
        
                    Container(
                        padding: EdgeInsets.only(left: 10),
                        height: 500,
                        child: ListView.builder(
                            itemCount: iconData.length,
                            itemBuilder: (context, index) => Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                    IconItem(
                                        iconData[index].id,
                                        iconData[index].title, 
                                        iconData[index].imageUrl, 
                                        iconData[index].isActive
                                    ),   
                                ]
                            ),
                        ),
                    )
                ]
            )     
        );        
    }
}