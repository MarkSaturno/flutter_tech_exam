import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:time_tracking_app/screens/business_card_screen.dart';
import 'package:time_tracking_app/utils/theme.dart';
import 'package:timetable/timetable.dart';

import '/providers/user_data.dart';
import 'providers/icon_data.dart';

import 'screens/add_time_recording_screen.dart';
import 'screens/calendar_screen.dart';
import '/screens/my_account_screen.dart';
import '/screens/pause_screen.dart';
import 'screens/time_recording_screen.dart';
import 'screens/login_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => IconsDrawer(),
        ),
        ChangeNotifierProvider(
          create: (_) => UserData(),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: defaultTheme,
        routes: {
          '/': (ctx) => LoginScreen(),
          MyAccount.routeName: (ctx) => MyAccount(),
          BusinessCard.routeName: (ctx) => BusinessCard(),
          TimeRecordingScreen.routeName: (ctx) => TimeRecordingScreen(),
          AddTimeRecording.routeName: (ctx) => AddTimeRecording(),
          PauseScreen.routeName: (ctx) => PauseScreen(),
          CalendarScreen.routeName: (ctx) => CalendarScreen(),
        },
        localizationsDelegates: [
          TimetableLocalizationsDelegate(),
        ],
      ),
    );
  }
}
