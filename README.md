# FFUF_TECH_EXAM

## Phone Emulator : Pixel XL API 30

# Packages

### [provider: ^5.0.0](https://pub.dev/packages/provider)

- A wrapper around InheritedWidget to make them easier to use and more reusable.

## [timetable: ^1.0.0-alpha.5](https://pub.dev/packages/timetable/versions/1.0.0-alpha.5)

- A customizable, animated calendar widget including day, week, and month views.

## [flutter_time_picker_spinner: ^2.0.0](https://pub.dev/packages/flutter_time_picker_spinner)

- Time Picker widget with spinner instead of a material time picker.

## [font_awesome_flutter: ^9.1.0](https://pub.dev/packages/font_awesome_flutter)

- The Font Awesome Icon pack available as set of Flutter Icons.

# App Features

## Drawer

- Display Scallable Icons
- Hover Effect

## Mein Konto

- Display UI

## Visitenkarte

- Reusable Tab View
- Clickable links

## Zeiterrfassung

- Display App Bar
- Working Kalendar and Add Zeiterrfassung icon

## Add Zeiterfassun

- Display UI
- Function in Mitarbeiter

# ScreenShots

## Login

![Anmelden.JPG](assets/screenshots/Anmelden.JPG)

## Drawer 

![Drawer1.JPG](assets/screenshots/Drawer1.JPG)

![Drawer2.JPG](assets/screenshots/Drawer2.JPG)


## Mein Konto

![MeinKonto_Scroll1.JPG](assets/screenshots/MeinKonto_Scroll1.JPG)

![MeinKonto_Scroll2.JPG](assets/screenshots/MeinKonto_Scroll2.JPG)

## Visitenkarte

![MeinVisikarte1.JPG](assets/screenshots/MeinVisikarte1.JPG)

![MeinVisikarte2.JPG](assets/screenshots/MeinVisikarte2.JPG)

![Vorgensetzte1.JPG](assets/screenshots/Vorgensetzte1.JPG)

![Vorgensetzte2.JPG](assets/screenshots/Vorgensetzte2.JPG)

## Zeiterrfassung

![Zeiterrfassung.JPG](assets/screenshots/Zeiterrfassung.JPG)

## Add Zeiterrfassung

![AddZeiterrfassung1.JPG](assets/screenshots/AddZeiterrfassung1.JPG)

![AddZeiterrfassung2.JPG](assets/screenshots/AddZeiterrfassung2.JPG)

![AddZeiterrfassung3.JPG](assets/screenshots/AddZeiterrfassung3.JPG)

![AddZeiterrfassung4.JPG](assets/screenshots/AddZeiterrfassung4.JPG)

![AddZeiterrfassung5.JPG](assets/screenshots/AddZeiterrfassung5.JPG)

![AddZeiterrfassung6.JPG](assets/screenshots/AddZeiterrfassung6.JPG)

![AddZeiterrfassung7.JPG](assets/screenshots/AddZeiterrfassung7.JPG)



